class SumaNums {

    public static void main(String[] args) {
        int total=0;
        int cont=0;
        int mayor = Integer.parseInt(args[0]);
        int menor = Integer.parseInt(args[0]);
        for (String s : args){
            int num = Integer.parseInt(s);
            cont ++;
            total += num;
            if (num > mayor){
                mayor = num;
            }
            if (num < menor){
                menor = num;
            }
        }

        double media = (double) total/cont;
        System.out.println(cont + " numeros introducidos");
        System.out.println("El total es "+ total);
        System.out.println("El numero mayor es " + mayor);
        System.out.println("El numero menor es " + menor);
        System.out.println("La media es "+ media);
    }
}
