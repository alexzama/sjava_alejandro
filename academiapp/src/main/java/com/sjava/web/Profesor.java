package com.sjava.web;

public class Profesor {

    private int id;
    private String nombre;
    private String email;
    private String telefono;
    private String especialidad;

    public Profesor(String nombre, String email, String telefono, String especialidad) {
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.especialidad = especialidad;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Profesor(int id, String nombre, String email, String telefono, String especialidad) {
        this.id = id;
        this.nombre = nombre;
        this.email = email;
        this.telefono = telefono;
        this.especialidad = especialidad;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getEmail(){
        return this.email;
    }

    protected void setEmail(String email){
        this.email = email;
    }

    public String getTelefono(){
        return this.telefono;
    }
    
    protected void setTelefono(String telefono){
        this.telefono = telefono;
    }

    public String getEspecialidad(){
        return this.especialidad;
    }
    
    protected void setEspecialidad(String especialidad){
        this.especialidad = especialidad;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }
}