package com.sjava.web;

public class Curso {

    private int id;
    private String descripcion;
    private int numHoras;
    private int idProfesor;

    public Curso(String descripcion, int numHoras, int idProfesor) {
        this.descripcion = descripcion;
        this.numHoras = numHoras;
        this.idProfesor = idProfesor;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Curso(int id, String descripcion, int numHoras, int idProfesor) {
        this.id = id;
        this.descripcion = descripcion;
        this.numHoras = numHoras;
        this.idProfesor = idProfesor;
    }

    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }
    
    public int getIdProfesor(){
        return this.idProfesor;
    }

    protected void setIdProfesor(int idProfesor){
        this.idProfesor=idProfesor;
    }

    public String getDescripcion(){
        return this.descripcion;
    }
    
    protected void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }


    public int getNumHoras(){
        return this.numHoras;
    }

    protected void setNumHoras(int numHoras){
        this.numHoras=numHoras;
    }

}