package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CursoController {
    private static List<Curso> listaCursos = new ArrayList<Curso>();
    private static int contador = 0;

    static {

        // Alumno a = new Alumno(99,"ricard hernández", "algo@algo.com", "999333");
        // listaAlumnos.add(a);
    }

    // getAll devuelve la lista completa
    public static List<Curso> getAll(){
        return listaCursos;
    }

    //getId devuelve un registro
    public static Curso getId(int id){
        for (Curso c : listaCursos) {
            if (c.getId()==id){
                return c;
            }
        }
        return null;
    }
   
    //save guarda un curso
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Curso c) {
        if (c.getId() == 0){
            contador++;
            c.setId(contador);
            listaCursos.add(c);
        } else {
            for (Curso cu : listaCursos) {
                if (cu.getId()==c.getId()) {
                   
                    cu.setDescripcion(c.getDescripcion());
                    cu.setNumHoras(c.getNumHoras());
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de cursos
    public static int size() {
        return listaCursos.size();
    }


    // removeId elimina curso por id
    public static void removeId(int id){
        Curso borrar=null; 
        for (Curso c : listaCursos) {
            if (c.getId()==id){
                borrar = c;
                break;
            }
        }
        if (borrar!=null) {
            listaCursos.remove(borrar);
        }
    }

}