<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AcademiaApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/academiapp/css/estilos.css">
</head>
<body>

<%@include file="/parts/menu.html"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Listado</h1>
</div>
</div>


<div class="row">
<div class="col">


<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Email</th>
      <th scope="col">Teléfono</th>
      <th scope="col">Especialidad</th>
     <th scope="col"></th>
     <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
   <% for (Profesor p : ProfesorController.getAll()) { %>
        <tr>
        <th scope="row"><%= p.getId() %></th>
        <td><%= p.getNombre() %></td>
        <td><%= p.getEmail() %></td>
        <td><%= p.getTelefono() %></td>
        <td><%= p.getEspecialidad() %></td>
        <td><a href="/academiapp/profesor/edit.jsp?id=<%= p.getId() %>">Edita</a></td>
        <td><a href="/academiapp/profesor/remove.jsp?id=<%= p.getId() %>">Elimina</a></td>
        </tr>
    <% } %>
  </tbody>
</table>

<a href="/academiapp/profesor/create.jsp" class="btn btn-sm btn-primary">Nuevo profesor</a>
</div>
</div>


</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>


</body>
</html>