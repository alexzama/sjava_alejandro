import java.util.Scanner;
class Teclado {

    public static int pideNumero(int min, int max){
        int num;
        Scanner keyboard = new Scanner(System.in);
        do{
            System.out.print("Introduce un numero: ");
            num = keyboard.nextInt();
        }while(num < min || num > max);
        return num;
    }
    public static String pideTexto(){
        String texto;
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Introduce un texto: ");
        texto = keyboard.nextLine();
        return texto;
    }

}