import java.util.Scanner;

class Main {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String  frase;
        String resultado;
        int min = 1;
        int max = 10;
        int resutNum;
        String texto;
        
        System.out.print("Introduce una frase: ");
        frase = keyboard.nextLine();

        resultado = Textos.slug(frase);
        resutNum = Teclado.pideNumero(min, max);
        texto = Teclado.pideTexto();

        System.out.println(resultado);
        System.out.println(resutNum);
        System.out.println(texto);
   
    }
}