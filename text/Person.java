
class Person {

    String nombre;
    int edad;

    public Person(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    public void compararPersona(Person otra){
        if (this.edad > otra.edad) {
            System.out.println ("La edad "+ this.edad+" es la mas alta");
        } else {
            System.out.println ("La edad "+ otra.edad+" es la mas alta");
        }
    }
    public void compararNombre (Person otra){
        if (this.nombre.equals(otra.nombre)){
            System.out.println("Los nombres son iguales");
        }
        else {
            System.out.println("No son iguales");
        }
    }

}