import java.util.Scanner;
class InfoNumsK {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num=1;
        int mayor = 0;
        int menor = 0;
        int cont = 0;
        boolean comprobar = true;
        do {
                System.out.printf("Entra un num: ");
            try {
                num = keyboard.nextInt();
                if(num !=0){
                    total += num;
                    cont ++;
                    if (comprobar){
                        mayor = num;
                        menor = num;
                        comprobar = false;
                    }
                    else {
                        if(mayor < num){
                            mayor = num;
                        }
                        if(menor > num){
                            menor = num;
                        }
                    }
                }
            } 
            catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next(); 
            }
        } while (num != 0);
        double media = (double) total / cont;
        System.out.println("La suma es "+ total);
        System.out.println("Numeros introducidos "+ cont);
        System.out.println("El mayor es "+ mayor);
        System.out.println("El menor es "+ menor);
        System.out.println("La media es "+ media);
    }
}