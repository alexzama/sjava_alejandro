import java.util.Random;
import java.util.Scanner;
class Adivina {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        Random random = new Random();
        int incognita = random.nextInt(10)+1;
        int cont = 0;
        int num = 0;

        do {
            System.out.println("Introduce un numero entre el 1 y el 10: ");
            try {
                num = keyboard.nextInt();
                cont ++;
            } 
            catch (Exception e) {
                System.out.println("***Dato incorrecto introduce un número entre en 1 y el 10 ***");
                keyboard.next(); 
            }
        } while (num != incognita);

        System.out.println("Lo conseguiste el numero era: "+ incognita);
        System.out.println("Numeros de intentos: " + cont);
    }
}
    