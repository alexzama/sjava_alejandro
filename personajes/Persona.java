class Persona{
    
    protected String nombre;
    protected int edad;

    Persona (String nombre, int edad){
        this.edad=edad;
        this.nombre=nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public void setEdad (int edad){
        this.edad = edad;
    }

    public int getEdad(){
        return edad;
    }

    public String getNombre (){
        return nombre;
    }
}