import java.util.Scanner;
class Test {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String  nombre;
        String sexo;
        int edad;
        
        System.out.print("Nombre? ");
        nombre = keyboard.nextLine();

        System.out.print("H / M ? ");
        sexo = keyboard.nextLine();

        System.out.print("Edad? ");
        edad = keyboard.nextInt(); 

        Persona a = new Hombre (nombre, edad);
        Persona b = new Mujer(nombre, edad);

        if(sexo.equals("h") || sexo.equals("H")){
           ((Hombre)a).nombreHombre();
        }
        else{
            ((Mujer)b).nombreMujer();
        }
    }
}