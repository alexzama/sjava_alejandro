class Hombre extends Persona {

    public Hombre(String nombre, int edad){
        super(nombre, edad);
    }
    
    public void nombreHombre(){
        System.out.println("Hola, soy el Sr " + super.nombre  + " y tengo " + super.edad);
    }
}