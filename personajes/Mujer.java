class Mujer extends Persona {
    public Mujer(String nombre, int edad){
        super(nombre, edad);
    }
    public void nombreMujer(){
        System.out.println("Hola, soy la Sra " + super.nombre + " y tengo " + super.edad);
   }
}
