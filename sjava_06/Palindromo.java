import java.util.Scanner;
class Palindromo {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String  palabra;
        String palindromo;
        StringBuilder builder = new StringBuilder();
        
        System.out.print("Entra palabra: ");
        palabra = keyboard.nextLine();
           
        char [] cadena = palabra.toCharArray();

        for ( int i = (cadena.length-1) ; i >= 0 ; i--){
                builder.append(cadena[i]);
        }
        palindromo = builder.toString().toLowerCase();
        
        String resultado = palindromo.substring(0, 1).toUpperCase()+palindromo.substring(1); 
        
        System.out.println("Palindromo: " + resultado);
    }
}