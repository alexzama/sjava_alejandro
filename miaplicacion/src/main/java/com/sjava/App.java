package com.sjava;

public class App 
{
    public static void main( String[] args )
    {
        new Persona("ana", 1995, "ana@gmail.com");
        new Persona("claudia", 1993, "claudia@gmail.com");
        new Persona("alex", 1999, "alex@gmail.com");
        new Persona("rocio", 1995, "rocio@gmail.com");
        new Persona("juan", 1994, "juan@gmail.com");
        PersonaController.muestraContactos();
        System.out.println("Muestra ");
        PersonaController.muestraContactoId(4);
        System.out.println("Has borrado un contacto");
        PersonaController.borraContacto(2);
        PersonaController.muestraContactos();
    }
}

