package com.sjava;

public class Persona {

    private String nombre;
    private String email;
    private int anyo;
    private int id;

    public Persona(String nombre, int anyo, String email) {
        this.nombre = nombre;
        this.email = email;
        this.anyo = anyo;
        PersonaController.nuevoContacto(this);
    }
    public void setId (int id){
        this.id = id;
    }

    public String getNombre(){
        return this.nombre;
    }
    public int getId(){
        return this.id;
    }

    public String getEmail(){
        return this.email;
    }
  
    public int getAnyo(){
        return this.anyo;
    }

    @Override
    public String toString() {
        return String.format("%d : %s (%s)", this.id, this.nombre, this.email);
    }
  
}
