
package com.sjava;
import java.util.ArrayList;

public class PersonaController {
    private static ArrayList<Persona> contactos = new ArrayList<Persona>();
    private static int contador = 0; 

    public static void muestraContactos(){
        for (Persona p : contactos) {
            System.out.println(p);
        }
    }
    public static void muestraContactoId(int n){
        for (Persona p: contactos){
            int id = p.getId();
            if(id == n){
                System.out.println(p);
                return;
            }
        }
    }
    public static void borraContacto(int borra){
        for (Persona p: contactos){
            int id = p.getId();
            if(id == borra){
                contactos.remove(p);
                return;
            }
        }

    }
    public static ArrayList<Persona> getContactos(){
        return contactos;
    }
    public static void nuevoContacto(Persona pers) {
        contador ++;
        pers.setId(contador);
        contactos.add(pers);
    }
    public static int numContactos() {
        return contactos.size();
    }
}
