import java.util.Set;
import java.util.TreeSet;

public class Provincias {

	public static void main(String[] args) {
	
        Set<String> provinciasOrdenadas = new TreeSet<String>();

        Datos d = new Datos();
        String [] provincias = d.provincias();

        for (String s : provincias){
            provinciasOrdenadas.add(s);
        }
        for (String p : provinciasOrdenadas){
            System.out.println(p);
        }
    }
}